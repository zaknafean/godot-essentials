class_name CameraFollow3D extends Camera3D

@export var back_distance := 0.9
@export var height := 0.9
@export var view_focus_v_offset := 0.7
@export var heading_interpolation_grounded := 4.0
@export var heading_interpolation_airborne := 3.5

var target: Node3D:
	set(value):
		target = value
		
		if target is Node3D:
			last_forward_vector = target.basis.z
			
		set_physics_process(target is Node3D)
		
var last_forward_vector := Vector3.ZERO


func _ready():
	set_physics_process(target is Node3D)
	
	if target is Node3D:
		last_forward_vector = target.basis.z


# Called every frame when this state is active.
func _physics_process(delta: float):
	global_position = target.global_position
	var target_forward: Vector3 = target.basis.z
	
	if target.has_method("is_on_floor") and target.is_on_floor():
		last_forward_vector = last_forward_vector.slerp(target_forward, heading_interpolation_grounded * delta)
	else:
		last_forward_vector = last_forward_vector.slerp(target_forward, heading_interpolation_airborne * delta)
	
	global_position += last_forward_vector * -back_distance
	global_position.y += height
	
	look_at(target.global_position + Vector3.UP * view_focus_v_offset)


func start_follow(new_target: Node3D): 
	target = new_target
	
	set_physics_process(target is Node3D)


func stop_follow():
	set_physics_process(false)
