### This code creates a custom animation player that focuses on maintaining a consistent frame rate. Here are some potential use cases where this approach might be beneficial:
# Precise Animation Timing: In situations where precise control over the animation speed is crucial, this custom player allows you to define the FPS and ensure each frame is displayed for the intended duration. This can be helpful for synchronized animations, frame-by-frame effects, or integration with external timing systems.
# Game Mechanics Tied to Frame Rate: If your game mechanics rely on a specific frame rate (e.g., physics simulations, character movement tied to animation frames), this approach ensures consistent behavior regardless of frame rate fluctuations that might occur due to hardware limitations or performance variations.
# Custom Animation Playback: You could use this class to create custom animation playback behaviors that differ from the default Godot animation player functionality
###

@tool
class_name AnimationPlayerSnapper extends AnimationPlayer

@export var fps := 30.0

var timer : float

func _ready():
	## Animation won't automatically advance based on the engine's internal clock
	## You need to use advance() function to process the animation manually
	callback_mode_process = AnimationMixer.ANIMATION_CALLBACK_MODE_PROCESS_MANUAL 


func _process(delta):
	timer += delta
	
	var frame = 1.0 / fps
	
	if(timer > frame):
		advance(timer)
		timer = fmod(timer, frame)
		
