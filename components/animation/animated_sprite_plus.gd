class_name AnimatedSprite2DPlus extends AnimatedSprite2D


var playing := false


func play_bounceback(new_animation: String):
	if sprite_frames and sprite_frames.get_animation_names().has(new_animation):
		var frame_number = frame
		var current_total_frames = sprite_frames.frames.get_frame_count(animation)
		playing = false
		
		animation = new_animation
		
		var new_animation_total_frames = sprite_frames.frames.get_frame_count(new_animation)
		var new_frame = new_animation_total_frames - frame_number
		
		if not ([0, current_total_frames].has(frame_number)):
			frame = new_frame
			
		playing = true
	else:
		push_error("AnimatedSprite2DPlus: The play bounceback function cannot be executed, the new animation name does not exist in this sprite")
