@icon("res://components/behaviour/loot-tables/loot_table.svg")
class_name LootTable extends Node

enum ITEM_RARITY { COMMON, UNCOMMON, RARE, LEGENDARY } ## Expand here as to adjust it to your game requirements
enum PROBABILITY { WEIGHT, ROLL_TIER }

## The available items that will be used on a roll
@export var available_items: Array[LootTableItem] = []
## The type of probability technique to apply on a loot, weight is the common case and generate random decimals
## while each time sum the weight of the next item. The roll tier uses a max roll number and define a number range
## for each tier
@export var probability_type := PROBABILITY.WEIGHT

@export_group("Generation")
## When this is enabled items can be repeated for multiple rolls on this generation
@export var allow_duplicates := true
## Max picks of common items when it comes to roll
@export var common_picks := 3
## Max picks of uncommon items when it comes to roll
@export var uncommon_picks := 2
## Max picks of rare items when it comes to roll
@export var rare_picks := 1
## Max picks of legendary items when it comes to roll
@export var legendary_picks := 0

@export_group("Weight")
## The maximum items this weight generation can drop, more items requires more generation times so it's the way
## to adjust the probability.
@export var max_weight_items := 3
## A little help that is added to the total weight to allow drop more items increasing the chance.
@export var extra_weight_roll := 0.25
@export_group("Roll")
@export var max_roll_items := 3
## Each time a random number between 0 and max roll will be generated, based on this result if the number
## fits on one of the rarity roll ranges, items of this rarity will be picked randomly
@export var max_roll := 100.0:
	set(value):
		max_roll = clamp(value, 1.0, max(max_common_roll, max_uncommon_roll, max_rare_roll, max_legendary_roll))
		
@export var min_common_roll := 50.0
@export var max_common_roll := 100.0
@export var min_uncommon_roll := 6.0
@export var max_uncommon_roll := 20.0
@export var min_rare_roll := 2.0
@export var max_rare_roll := 5.0
@export var min_legendary_roll := 0.1
@export var max_legendary_roll := 1.0


func _init(items: Array[LootTableItem] = []):
	add_items(items)
	
	
func generate(
	times: int,
 	type: PROBABILITY = probability_type,
 	rarities: Array[ITEM_RARITY] = [ITEM_RARITY.COMMON, ITEM_RARITY.UNCOMMON, ITEM_RARITY.RARE, ITEM_RARITY.LEGENDARY]
) -> Array[LootTableItem]:
	var result: Array[LootTableItem] = []
	
	times = max(1, times)
	
	match type:
		PROBABILITY.WEIGHT:
			result = weight(times, rarities)
		PROBABILITY.ROLL_TIER:
			result = roll_tier(times, rarities)
	
	return result


func total_sum_weight() -> float:
	return available_items.reduce(func(accum: float, item: LootTableItem): return accum + item.weight, 0.0)


func weight(
	times: int = 1, 
	rarities: Array[ITEM_RARITY] = [ITEM_RARITY.COMMON, ITEM_RARITY.UNCOMMON, ITEM_RARITY.RARE, ITEM_RARITY.LEGENDARY]
	) -> Array[LootTableItem]:
	var result: Array[LootTableItem] = []
	var total_weight := 0.0
	var items_by_rarity = filter_by_rarities(rarities)
	items_by_rarity.shuffle()
	
	var max_picks = min(items_by_rarity.size(), max_weight_items)
	
	for item: LootTableItem in items_by_rarity:
		total_weight += item.weight
		item.accum_weight = total_weight
		
	total_weight += extra_weight_roll
	
	for i in range(times):
		if result.size() >= max_picks:
			break
			
		var roll := randf_range(0.0, total_weight)

		for item: LootTableItem in items_by_rarity:
			if roll >= item.accum_weight:
				result.append(item)
					
				if not allow_duplicates:
					items_by_rarity.erase(item)
					
				break
	
	return result.slice(0, max_picks)
				
	
func roll_tier(
	times: int = 1, 
	rarities: Array[ITEM_RARITY] = [ITEM_RARITY.COMMON, ITEM_RARITY.UNCOMMON, ITEM_RARITY.RARE, ITEM_RARITY.LEGENDARY]
	) -> Array[LootTableItem]:
	var result: Array[LootTableItem] = []

	for i in range(times):
		if result.size() >= max_roll_items:
			break
			
		var item_rarity_roll = randf_range(0.0, max_roll)

		if legendary_picks > 0 and ITEM_RARITY.LEGENDARY in rarities and item_rarity_roll >= min_legendary_roll and item_rarity_roll <= max_legendary_roll:
			result.append_array(pick_random_items(ITEM_RARITY.LEGENDARY, randi_range(0, legendary_picks)))
			
		elif rare_picks > 0 and ITEM_RARITY.RARE in rarities and item_rarity_roll >= min_rare_roll and item_rarity_roll <= max_rare_roll:
			result.append_array(pick_random_items(ITEM_RARITY.RARE, randi_range(0, rare_picks)))
			
		elif uncommon_picks > 0 and ITEM_RARITY.UNCOMMON in rarities and item_rarity_roll >= min_uncommon_roll and item_rarity_roll <= max_uncommon_roll:
			result.append_array(pick_random_items(ITEM_RARITY.UNCOMMON, randi_range(0, uncommon_picks)))
			
		elif common_picks > 0 and ITEM_RARITY.COMMON in rarities and item_rarity_roll >= min_common_roll and item_rarity_roll <= max_common_roll:
			result.append_array(pick_random_items(ITEM_RARITY.COMMON, randi_range(0, common_picks)))
	

	if not allow_duplicates:
		var no_duplicates_result: Array[LootTableItem] = []
		
		for item: LootTableItem in result:
			if no_duplicates_result.has(item):
				no_duplicates_result.append(item)
				
		return no_duplicates_result
		
	return result.slice(0, max_roll_items)


func pick_random_items(rarity: ITEM_RARITY, amount: int = 1) -> Array[LootTableItem]:
	var items_by_rarity := filter_by_rarity(rarity)
	var items: Array[LootTableItem] = []
	
	if items_by_rarity.is_empty() or amount == 0:
		return items
	
	for i in range(amount):
		if not allow_duplicates and items_by_rarity.is_empty():
			return items
			
		var result_item = items_by_rarity.pick_random()
		
		if result_item is LootTableItem:
			if not allow_duplicates:
				items_by_rarity.erase(result_item)
			
			items.append(result_item)
		
	return items


func filter_by_rarities(rarities: Array[ITEM_RARITY] = []) -> Array[LootTableItem]:
	return available_items.filter(func(item: LootTableItem): return item.rarity in rarities)


func filter_by_rarity(rarity: ITEM_RARITY) -> Array[LootTableItem]:
	return available_items.filter(func(item: LootTableItem): return item.rarity == rarity)


func add_items(items: Array[LootTableItem] = []) -> void:
	available_items.append_array(items)


func add_item(item: LootTableItem) -> void:
	available_items.append(item)


func remove_items(items: Array[LootTableItem] = []) -> void:
	available_items = available_items.filter(func(item: LootTableItem): return not item in items)


func remove_item(item: LootTableItem) -> void:
	available_items.erase(item)


func remove_items_by_id(item_ids: Array[StringName] = []) -> void:
	available_items = available_items.filter(func(item: LootTableItem): return not item.id in item_ids)


func remove_item_by_id(item_id: StringName) -> void:
	available_items  = available_items.filter(func(item: LootTableItem): return not item.id == item_id)
