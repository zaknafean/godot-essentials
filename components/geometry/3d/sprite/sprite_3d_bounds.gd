class_name Sprite3DBounds extends Node3D

@export var sprite: Sprite3D

var bound_rect: Rect2


func _ready():
	calculate_texture_bounds()
	hide()


func calculate_texture_bounds():
	var size := sprite.texture.get_size() * sprite.pixel_size * VectorWizard.get_topdown_vector(scale)

	var from = VectorWizard.get_topdown_vector(sprite.global_position) - size / 2.0
	var to = VectorWizard.get_topdown_vector(sprite.global_position) + size / 2.0
	
	bound_rect = Rect2(from, to - from)


## returns the position of a node in relation to the bounds, proportional to the size (0,0 is top-left, 1,1 is bottom-right)
func get_relative_position(node: Node3D) -> Vector2:
	var relative_position := VectorWizard.get_topdown_vector(node.global_position) - bound_rect.position
	var proportional_position := relative_position / bound_rect.size
	
	return proportional_position


func get_texture() -> Texture2D:
	if sprite:
		return sprite.texture
		
	return null
