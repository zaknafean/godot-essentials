class_name PieceResource extends Resource

@export var texture: Texture2D
@export var block_type := BLOCK_TYPE.NORMAL
@export var group_name := "match3-piece"

enum BLOCK_TYPE {
	NORMAL,
	SPECIAL,
	OBSTACLE,
}


func is_normal() -> bool:
	return block_type == BLOCK_TYPE.NORMAL
	
	
func is_special() -> bool:
	return block_type == BLOCK_TYPE.SPECIAL


func is_obstacle() -> bool:
	return block_type == BLOCK_TYPE.OBSTACLE
