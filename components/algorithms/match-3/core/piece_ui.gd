@tool
class_name Piece extends TextureRect

signal locked
signal unlocked
signal dragged(at_position: Vector2)
signal drag_ended()

var data: PieceResource
var line_swap := false
var adjacent_swap := true

var cell

var is_locked := false:
	set(value):
		if value != is_locked:
			if value:
				locked.emit()
			else:
				unlocked.emit()
		
		is_locked = value

var on_drag := false


func _enter_tree():
	add_to_group(data.group_name)
	
	
func _ready():
	if cell == null:
		push_error("The piece %s does not have a GridCell linked" % name)
	
	texture = data.texture
	expand_mode = TextureRect.EXPAND_IGNORE_SIZE
	custom_minimum_size = cell.cell_size


func _notification(notification_type):
	match notification_type:
		NOTIFICATION_DRAG_END:
			if on_drag:
				show()
				cell.board.line_piece_drag_record.clear()
				
			on_drag = false


func _get_drag_data(_at_position: Vector2):
	if line_swap:
		var parent_node_preview := Control.new()
		var piece_preview := TextureRect.new()
		
		piece_preview.texture = texture
		piece_preview.expand_mode = expand_mode
		piece_preview.custom_minimum_size = custom_minimum_size

		parent_node_preview.add_child(piece_preview)
		piece_preview.position = -cell.cell_size / 2
		set_drag_preview(parent_node_preview)
		
		on_drag = true
		dragged.emit(self)
		
		add_to_drag_record()
		hide()
		
		return self


func _can_drop_data(_at_position: Vector2, _data):
	var dragged_piece := _data as Piece
	
	if line_swap and (in_same_row_as(dragged_piece) or in_same_column_as(dragged_piece)):
		add_to_drag_record()
		return true
		
	return adjacent_swap and is_neighbour_of(dragged_piece)
			

func _drop_data(_at_position: Vector2, _data):
	var dragged_piece := _data as Piece
	
	print("DROPPED ", data.name)
	
	

func add_to_drag_record():
	if not cell.board.line_piece_drag_record.has(self):
		var last_dragged_piece = null if cell.board.line_piece_drag_record.is_empty() else cell.board.line_piece_drag_record.back()
		cell.board.line_piece_drag_record.append(self)
		
		if last_dragged_piece is Piece:
			var tween = create_tween()
			tween.tween_property(self, "position", last_dragged_piece.cell.grid_position, 0.3).set_ease(Tween.EASE_IN_OUT)
	
	
func is_neighbour_of(piece: Piece):
	var left_column: int = cell.column - 1
	var right_column: int = cell.column + 1
	var upper_row: int = cell.row - 1
	var bottom_row: int = cell.row + 1
	
	return (in_same_row_as(piece) and piece.cell.column in [left_column, right_column]) \
		or (in_same_column_as(piece) and piece.cell.row in [upper_row, bottom_row])


func in_same_row_as(piece: Piece):
	return piece.cell.row == cell.row
		

func in_same_column_as(piece: Piece):
	return piece.cell.column == cell.column
	

func lock():
	is_locked = true

func unlock():
	is_locked = false


func is_normal() -> bool:
	return data.is_normal()
	
	
func is_special() -> bool:
	return data.is_special()


func is_obstacle() -> bool:
	return data.is_obstacle()

