@tool
class_name Match3Board extends Control

const MIN_GRID_WIDTH = 3
const MIN_GRID_HEIGHT = 3

const PIECE_UI = preload("res://components/algorithms/match-3/core/piece.tscn")

@export_group("Editor")
@export var debug_mode := false
@export var clean_grid := false:
	set(value):
		clean_grid = false
		
		if Engine.is_editor_hint():
			NodeWizard.remove_and_queue_free_children(self)

@export_group("Board parameters")
@export var swap_mode := SWAP_MODE.ADJACENT
## The grid width
@export var grid_width := 8:
	set(value):
		grid_width = max(MIN_GRID_WIDTH, value)
		
		if debug_mode and Engine.is_editor_hint():
			prepare_board(grid_width, grid_height)

## The grid height
@export var grid_height := 7:
	set(value):
		grid_height = max(MIN_GRID_HEIGHT, value)
		
		if debug_mode and Engine.is_editor_hint():
			prepare_board(grid_width, grid_height)

## The size of each cell, recommended that the size be multiples of 2
@export var cell_size := Vector2(32, 32):
	set(value):
		cell_size = value
		
		if debug_mode and Engine.is_editor_hint():
			prepare_board(grid_width, grid_height)
## The separation between cells where 'x' represents horizontal and 'y' vertical
@export var offset := Vector2(5, 10):
	set(value):
		offset = value
		
		if debug_mode and Engine.is_editor_hint():
			prepare_board(grid_width, grid_height)
## The collection of custom piece resources to be available in the board
@export var piece_collection: Array[PieceResource] = []

enum SWAP_MODE {
	ADJACENT,
	LINE
}

var board := []
var line_piece_drag_record: Array[Piece] = []


func _ready():
	assert(piece_collection.size() > 0, "Match3: The Match-3 Board at least need 2 different pieces to be playable")
	
	prepare_board(grid_width, grid_height)

	
func prepare_board(width = MIN_GRID_WIDTH, height = MIN_GRID_HEIGHT):
	NodeWizard.remove_and_queue_free_children(self)
		
	board.clear()
	
	for column_cell in range(width):
		board.append([])
		
		for row_cell in range(height):
			var cell_position := Vector2(cell_size.x * column_cell + (offset.x * column_cell), cell_size.y * row_cell + (offset.y * row_cell))
			var piece := PIECE_UI.instantiate() as Piece
			
			piece.name = "Piece_Column%d_Row%d" % [column_cell, row_cell]
			piece.position = cell_position
			piece.data = pieces_without_obstacles().pick_random()
			piece.adjacent_swap = swap_mode == SWAP_MODE.ADJACENT
			piece.line_swap = swap_mode == SWAP_MODE.LINE
			
			board[column_cell].append(GridCell.new(self, cell_position, cell_size, column_cell, row_cell, piece))
			add_child(piece)
			
			NodeWizard.set_owner_to_edited_scene_root(piece)


func pieces_without_obstacles() -> Array[PieceResource]:
	return piece_collection.filter(func(piece: PieceResource): return not piece.is_obstacle())


class GridCell:
	var board: Match3Board
	var grid_position: Vector2
	var cell_size: Vector2
	var column: int
	var row: int
	var piece: Piece
	
	func _init(_board: Match3Board, _position: Vector2, _cell_size: Vector2, _column: int, _row: int, _piece: Piece):
		board = _board
		grid_position = _position
		cell_size = _cell_size
		column = _column
		row = _row
		piece = _piece
		
		piece.cell = self
