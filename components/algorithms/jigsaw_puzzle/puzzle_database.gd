class_name PuzzleDatabase

const _462317_BE_5D_5A_416F_BA_27_5_AB_08B_1_FA_91B = preload("res://components/algorithms/jigsaw_puzzle/_462317be-5d5a-416f-ba27-5ab08b1fa91b.jpg")

static var available_puzzles := {
	"zen": {
		"old_photography": _462317_BE_5D_5A_416F_BA_27_5_AB_08B_1_FA_91B,
	},
}



static func fetch_puzzle(category: String, id: String) -> Texture2D:
	if available_puzzles.has(category) and available_puzzles[category].has(id):
		return available_puzzles[category][id] as Texture2D
		
	push_error("PuzzleDatabase: Trying to fetch a puzzle with category %s and id %s but does not exist" % [category, id])
	
	return null
