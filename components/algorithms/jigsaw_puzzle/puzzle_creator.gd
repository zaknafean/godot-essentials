class_name PuzzleCreator extends Node

enum PUZZLE_SIZES {
	CUSTOM,
	PIECES_20,
	PIECES_40,
	PIECES_60,
	PIECES_80,
	PIECES_130,
	PIECES_180,
	PIECES_285,
	PIECES_320,
	PIECES_500
}

enum PIECE_MASK_MODE {
	BLIT_RECT,
	SHADER
}

enum FIXED_SCALE {
	YES,
	NO
}


@export var piece_mask_mode := PIECE_MASK_MODE.BLIT_RECT
@export var fixed_piece_scale := FIXED_SCALE.YES
@export var piece_fixed_size := Vector2(95.5, 95.5)
@export var puzzle_texture: Texture2D
@export var puzzle_size := PUZZLE_SIZES.CUSTOM
@export var puzzle_width := 13
@export var puzzle_height := 10

@onready var pieces_box: Panel = $"../PiecesBox"


var current_puzzle_grid := []
var puzzle_image: Image

var mask_shader_material := {}



func prepare_puzzle_image() -> void:
	puzzle_image = puzzle_texture.get_image()
	puzzle_image.fix_alpha_edges()
	puzzle_image.decompress()
	puzzle_image.convert(Image.FORMAT_RGBA8)


func _ready():
	load_puzzle("zen", "old_photography")
	prepare_puzzle_image()
	
	current_puzzle_grid.clear()
	
	var selected_puzzle_size = _size_by_enum(puzzle_size)
	var puzzle_piece_dimensions := calculate_piece_dimensions(puzzle_image, selected_puzzle_size.x, selected_puzzle_size.y)
	
	for column in range(selected_puzzle_size.x):
		var column_position = Vector2(puzzle_piece_dimensions.width * column, puzzle_piece_dimensions.height)
		
		current_puzzle_grid.append([])
		
		for row in range(selected_puzzle_size.y):
			var puzzle_piece := create_puzzle_piece(column_position, column, row, puzzle_piece_dimensions, selected_puzzle_size)
			
			current_puzzle_grid[column].append(puzzle_piece)
			puzzle_piece.set_grid(current_puzzle_grid)
			
	## We add the piece as a child after the grid preparation to detect the neighbours correctly

	for piece: PuzzlePiece in ArrayWizard.flatten(current_puzzle_grid):
		add_piece_to_panel(piece)
		

func add_piece_to_panel(new_piece: PuzzlePiece):
	pieces_box.add_child(new_piece)
	new_piece.position = VectorWizard.random_point_in_rect(pieces_box.get_rect()) * new_piece.scale
	new_piece.position.y -= new_piece.size.y 
	new_piece.position.x = clamp(new_piece.position.x, 0, pieces_box.get_rect().size.x - new_piece.size.x)
	
	if fixed_piece_scale == FIXED_SCALE.YES:
		var scale_width = snappedf(piece_fixed_size.x / new_piece.texture_size().x, 0.0001)
		var scale_height = snappedf(piece_fixed_size.y / new_piece.texture_size().y, 0.0001)
		new_piece.scale *= Vector2(scale_width, scale_height)
		
	
func create_puzzle_piece(column_position: Vector2, column: int, row: int, dimensions: PuzzlePieceDimensions, _puzzle_size: Vector2) -> PuzzlePiece:
	var row_position := Vector2(column_position.x, dimensions.height * row)
	var puzzle_mask: JigsawPiecesLoader.PuzzleMask = choose_mask_texture(column, row, _puzzle_size)
	
	var mask_image: Image = puzzle_mask.texture.get_image()
	var mask_size := dimensions.to_vector()

	mask_image.resize(dimensions.width, dimensions.height)

	var puzzle_shape_image = Image.create(dimensions.width, dimensions.height, false, mask_image.get_format())
	puzzle_shape_image.fill(Color.TRANSPARENT)

	match piece_mask_mode:
		PIECE_MASK_MODE.BLIT_RECT:
			puzzle_shape_image.blit_rect_mask(
				puzzle_image.get_region(Rect2i(row_position, mask_size)), 
				mask_image, 
				Rect2i(Vector2i.ZERO, mask_size), 
				Vector2i.ZERO
			)
		
		PIECE_MASK_MODE.SHADER:
			puzzle_shape_image.blit_rect(
				puzzle_image.get_region(Rect2i(row_position, mask_size)), 
				Rect2i(Vector2i.ZERO, mask_size), 
				Vector2i.ZERO
			)
	
	var puzzle_piece = PuzzlePiece.new(column, row, _puzzle_size, ImageTexture.create_from_image(puzzle_shape_image), puzzle_mask)
	
	if piece_mask_mode == PIECE_MASK_MODE.SHADER:
		if mask_shader_material.has(puzzle_mask.texture):
			puzzle_piece.material = mask_shader_material[puzzle_mask.texture]
		else:
			puzzle_piece.material = JigsawPiecesLoader.PUZZLE_PIECE_MASK_MATERIAL.duplicate() as ShaderMaterial
			puzzle_piece.material.set_shader_parameter("mask", puzzle_mask.texture)
			mask_shader_material[puzzle_mask.texture] = puzzle_piece.material

	return puzzle_piece


func load_puzzle(category: String, id: String):
	puzzle_texture = PuzzleDatabase.fetch_puzzle(category, id)
	
	assert(puzzle_texture is Texture2D, "PuzzleCreator: The given puzzle texture is not valid or corrupted %s" % puzzle_texture)
	
	
func calculate_piece_dimensions(puzzle: Image, width: int = puzzle_width, height: int = puzzle_height) -> PuzzlePieceDimensions:
	var size = puzzle.get_size()
	
	var piece_width = floor(size.x / width)
	var piece_height = floor(size.y / height)

	return PuzzlePieceDimensions.new(piece_width, piece_height)


## This function works as long as the puzzle is filled in column top-bottom direction from left to right
func choose_mask_texture(column: int, row: int, size: Vector2) -> JigsawPiecesLoader.PuzzleMask:
	if _is_top_left_corner(column, row, size):
		return JigsawPiecesLoader.top_left_corner_pieces().pick_random()
	
	## Columns 0 means that is filling the border left part
	if column == 0:
		var previous_piece: PuzzlePiece = current_puzzle_grid[column][row - 1]
		
		if _is_bottom_left_corner(column, row, size):
			return JigsawPiecesLoader.bottom_left_corner_pieces().filter(
				func(mask: JigsawPiecesLoader.PuzzleMask):
					return mask.vertex.top != previous_piece.puzzle_mask.vertex.bottom
					).pick_random()
		else:
			return JigsawPiecesLoader.border_left_pieces().filter(
				func(mask: JigsawPiecesLoader.PuzzleMask):
					return mask.vertex.top != previous_piece.puzzle_mask.vertex.bottom
					).pick_random()
					
	elif column < size.x - 1: ## This is the interior pieces part
		if _is_border_top(column, row, size):
			var previous_piece = current_puzzle_grid[column - 1][row]
			
			return JigsawPiecesLoader.border_top_pieces().filter(
				func(mask: JigsawPiecesLoader.PuzzleMask):
					return mask.vertex.left != previous_piece.puzzle_mask.vertex.right
					).pick_random()
					
		elif _is_border_bottom(column, row, size):
			var previous_left_piece = current_puzzle_grid[column - 1][row]
			var previous_top_piece = current_puzzle_grid[column][row - 1]
			
			return JigsawPiecesLoader.border_bottom_pieces().filter(
				func(mask: JigsawPiecesLoader.PuzzleMask):
					return mask.vertex.top != previous_top_piece.puzzle_mask.vertex.bottom \
						and mask.vertex.left != previous_left_piece.puzzle_mask.vertex.right
					).pick_random()
		else:
			var previous_left_piece = current_puzzle_grid[column - 1][row]
			var previous_top_piece = current_puzzle_grid[column][row - 1]
			
			return JigsawPiecesLoader.interior_pieces().filter(
				func(mask: JigsawPiecesLoader.PuzzleMask):
					return mask.vertex.top != previous_top_piece.puzzle_mask.vertex.bottom \
						and mask.vertex.left != previous_left_piece.puzzle_mask.vertex.right
					).pick_random()
			
					
	else: ## Last column means that is filling the border right part
		var previous_left_piece: PuzzlePiece = current_puzzle_grid[column - 1][row]
		
		if _is_top_right_corner(column, row, size):
			return JigsawPiecesLoader.top_right_corner_pieces().filter(
				func(mask: JigsawPiecesLoader.PuzzleMask):
					return mask.vertex.left != previous_left_piece.puzzle_mask.vertex.right
					).pick_random()
					
		elif _is_bottom_right_corner(column, row, size):
			var previous_top_piece = current_puzzle_grid[column][row - 1]
			
			return JigsawPiecesLoader.bottom_right_corner_pieces().filter(
				func(mask: JigsawPiecesLoader.PuzzleMask):
					return mask.vertex.top != previous_top_piece.puzzle_mask.vertex.bottom \
						and mask.vertex.left != previous_left_piece.puzzle_mask.vertex.right
					).pick_random()
					
		else:
			var previous_top_piece = current_puzzle_grid[column][row - 1]
			
			return JigsawPiecesLoader.border_right_pieces().filter(
				func(mask: JigsawPiecesLoader.PuzzleMask):
					return mask.vertex.top != previous_top_piece.puzzle_mask.vertex.bottom \
						and mask.vertex.left != previous_left_piece.puzzle_mask.vertex.right
					).pick_random()


func _is_border_top(column: int, row: int, _puzzle_size: Vector2) -> bool:
	return column > 0 and column < _puzzle_size.x - 1 and row == 0


func _is_border_bottom(column: int, row: int, _puzzle_size: Vector2) -> bool:
	return column > 0 and column < _puzzle_size.x - 1 and row == _puzzle_size.y - 1


func _is_border_left(column: int, row: int, _puzzle_size: Vector2) -> bool:
	return row > 0 and row < _puzzle_size.y - 1 and column == 0


func _is_border_right(column: int, row: int, _puzzle_size: Vector2) -> bool:
	return row > 0 and row < _puzzle_size.y - 1 and column == _puzzle_size.x - 1


func _is_top_left_corner(column: int, row: int, _puzzle_size: Vector2) -> bool:
	return column == 0 and row == 0
	

func _is_bottom_left_corner(column: int, row: int, _puzzle_size: Vector2) -> bool:
	return column == 0 and row == _puzzle_size.y - 1
	

func _is_top_right_corner(column: int, row: int, _puzzle_size: Vector2) -> bool:
	return column == _puzzle_size.x - 1 and row == 0
	

func _is_bottom_right_corner(column: int, row: int, _puzzle_size: Vector2) -> bool:
	return column == _puzzle_size.x - 1 and row == _puzzle_size.y - 1


func _is_border(column: int, row: int, _puzzle_size: Vector2) -> bool:
	return row == 0 or row == _puzzle_size.y - 1 or column == 0 or column == _puzzle_size.x - 1


func _is_corner(column: int, row: int, _puzzle_size: Vector2) -> bool:
	return _is_bottom_left_corner(column, row, _puzzle_size) \
		or _is_bottom_right_corner(column, row, _puzzle_size) \
		or _is_top_left_corner(column, row, _puzzle_size) \
		or _is_top_right_corner(column, row, _puzzle_size)


func _size_by_enum(selected_puzzle_size: PUZZLE_SIZES) -> Vector2:
	var result := Vector2(puzzle_width, puzzle_height)
	var size = puzzle_image.get_size()
	
	match selected_puzzle_size:
		PUZZLE_SIZES.CUSTOM:
			result = Vector2(puzzle_width, puzzle_height)
		PUZZLE_SIZES.PIECES_20:
			result = Vector2(5, 4)
		PUZZLE_SIZES.PIECES_40:
			result = Vector2(8, 5)
		PUZZLE_SIZES.PIECES_60:
			result = Vector2(10, 6)
		PUZZLE_SIZES.PIECES_80:
			result = Vector2(10, 8)
		PUZZLE_SIZES.PIECES_130:
			result = Vector2(13, 10)
		PUZZLE_SIZES.PIECES_180:
			result = Vector2(15, 12)
		PUZZLE_SIZES.PIECES_285:
			result = Vector2(19, 15)
		PUZZLE_SIZES.PIECES_320:
			result = Vector2(20, 16)
		PUZZLE_SIZES.PIECES_500:
			result = Vector2(25, 20)
	
	## In case the puzzle image is more taller than wide
	return result if size.x > size.y else Vector2(result.y, result.x)
	

class PuzzlePieceDimensions:
	var height: int
	var width: int
	var short_side: int
	var number_of_pieces: int
	
	func _init(_width: int, _height: int):
		width = _width
		height = _height
		short_side = min(width, height)
		number_of_pieces = width * height


	func to_vector() -> Vector2:
		return Vector2(width, height)


	func to_vector_i() -> Vector2:
		return Vector2i(width, height)
