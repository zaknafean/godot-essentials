extends Node

const CORNER_TOP_RIGHT_001 = preload("res://components/algorithms/jigsaw_puzzle/assets/png_pieces/CornerTopRight001.PNG")
const CORNER_TOP_RIGHT_002 = preload("res://components/algorithms/jigsaw_puzzle/assets/png_pieces/CornerTopRight002.PNG")
const CORNER_TOP_RIGHT_003 = preload("res://components/algorithms/jigsaw_puzzle/assets/png_pieces/CornerTopRight003.PNG")
const CORNER_TOP_RIGHT_004 = preload("res://components/algorithms/jigsaw_puzzle/assets/png_pieces/CornerTopRight004.PNG")
const CORNER_TOP_LEFT_001 = preload("res://components/algorithms/jigsaw_puzzle/assets/png_pieces/CornerTopLeft001.PNG")
const BORDER_TOP_001 = preload("res://components/algorithms/jigsaw_puzzle/assets/png_pieces/BorderTop001.PNG")
const BORDER_TOP_002 = preload("res://components/algorithms/jigsaw_puzzle/assets/png_pieces/BorderTop002.PNG")
const CORNER_TOP_LEFT_002 = preload("res://components/algorithms/jigsaw_puzzle/assets/png_pieces/CornerTopLeft002.PNG")
const BORDER_TOP_003 = preload("res://components/algorithms/jigsaw_puzzle/assets/png_pieces/BorderTop003.PNG")
const BORDER_TOP_004 = preload("res://components/algorithms/jigsaw_puzzle/assets/png_pieces/BorderTop004.PNG")
const CORNER_TOP_LEFT_003 = preload("res://components/algorithms/jigsaw_puzzle/assets/png_pieces/CornerTopLeft003.PNG")
const BORDER_TOP_005 = preload("res://components/algorithms/jigsaw_puzzle/assets/png_pieces/BorderTop005.PNG")
const BORDER_TOP_006 = preload("res://components/algorithms/jigsaw_puzzle/assets/png_pieces/BorderTop006.PNG")
const CORNER_TOP_LEFT_004 = preload("res://components/algorithms/jigsaw_puzzle/assets/png_pieces/CornerTopLeft004.PNG")
const BORDER_TOP_007 = preload("res://components/algorithms/jigsaw_puzzle/assets/png_pieces/BorderTop007.PNG")
const BORDER_TOP_008 = preload("res://components/algorithms/jigsaw_puzzle/assets/png_pieces/BorderTop008.PNG")
const CORNER_BOTTOM_RIGHT_001 = preload("res://components/algorithms/jigsaw_puzzle/assets/png_pieces/CornerBottomRight001.PNG")
const CORNER_BOTTOM_RIGHT_002 = preload("res://components/algorithms/jigsaw_puzzle/assets/png_pieces/CornerBottomRight002.PNG")
const BORDER_RIGHT_001 = preload("res://components/algorithms/jigsaw_puzzle/assets/png_pieces/BorderRight001.PNG")
const BORDER_RIGHT_002 = preload("res://components/algorithms/jigsaw_puzzle/assets/png_pieces/BorderRight002.PNG")
const BORDER_RIGHT_003 = preload("res://components/algorithms/jigsaw_puzzle/assets/png_pieces/BorderRight003.PNG")
const BORDER_RIGHT_004 = preload("res://components/algorithms/jigsaw_puzzle/assets/png_pieces/BorderRight004.PNG")
const CORNER_BOTTOM_LEFT_001 = preload("res://components/algorithms/jigsaw_puzzle/assets/png_pieces/CornerBottomLeft001.PNG")
const BORDER_BOTTOM_001 = preload("res://components/algorithms/jigsaw_puzzle/assets/png_pieces/BorderBottom001.PNG")
const BORDER_BOTTOM_002 = preload("res://components/algorithms/jigsaw_puzzle/assets/png_pieces/BorderBottom002.PNG")
const BORDER_LEFT_001 = preload("res://components/algorithms/jigsaw_puzzle/assets/png_pieces/BorderLeft001.PNG")
const INTERIOR_001 = preload("res://components/algorithms/jigsaw_puzzle/assets/png_pieces/Interior001.PNG")
const INTERIOR_002 = preload("res://components/algorithms/jigsaw_puzzle/assets/png_pieces/Interior002.PNG")
const BORDER_LEFT_002 = preload("res://components/algorithms/jigsaw_puzzle/assets/png_pieces/BorderLeft002.PNG")
const INTERIOR_003 = preload("res://components/algorithms/jigsaw_puzzle/assets/png_pieces/Interior003.PNG")
const INTERIOR_004 = preload("res://components/algorithms/jigsaw_puzzle/assets/png_pieces/Interior004.PNG")
const CORNER_BOTTOM_LEFT_002 = preload("res://components/algorithms/jigsaw_puzzle/assets/png_pieces/CornerBottomLeft002.PNG")
const BORDER_BOTTOM_003 = preload("res://components/algorithms/jigsaw_puzzle/assets/png_pieces/BorderBottom003.PNG")
const BORDER_BOTTOM_004 = preload("res://components/algorithms/jigsaw_puzzle/assets/png_pieces/BorderBottom004.PNG")
const BORDER_LEFT_003 = preload("res://components/algorithms/jigsaw_puzzle/assets/png_pieces/BorderLeft003.PNG")
const INTERIOR_005 = preload("res://components/algorithms/jigsaw_puzzle/assets/png_pieces/Interior005.PNG")
const INTERIOR_006 = preload("res://components/algorithms/jigsaw_puzzle/assets/png_pieces/Interior006.PNG")
const BORDER_LEFT_004 = preload("res://components/algorithms/jigsaw_puzzle/assets/png_pieces/BorderLeft004.PNG")
const INTERIOR_007 = preload("res://components/algorithms/jigsaw_puzzle/assets/png_pieces/Interior007.PNG")
const INTERIOR_008 = preload("res://components/algorithms/jigsaw_puzzle/assets/png_pieces/Interior008.PNG")
const CORNER_BOTTOM_RIGHT_003 = preload("res://components/algorithms/jigsaw_puzzle/assets/png_pieces/CornerBottomRight003.PNG")
const CORNER_BOTTOM_RIGHT_004 = preload("res://components/algorithms/jigsaw_puzzle/assets/png_pieces/CornerBottomRight004.PNG")
const BORDER_RIGHT_005 = preload("res://components/algorithms/jigsaw_puzzle/assets/png_pieces/BorderRight005.PNG")
const BORDER_RIGHT_006 = preload("res://components/algorithms/jigsaw_puzzle/assets/png_pieces/BorderRight006.PNG")
const BORDER_RIGHT_007 = preload("res://components/algorithms/jigsaw_puzzle/assets/png_pieces/BorderRight007.PNG")
const BORDER_RIGHT_008 = preload("res://components/algorithms/jigsaw_puzzle/assets/png_pieces/BorderRight008.PNG")
const CORNER_BOTTOM_LEFT_003 = preload("res://components/algorithms/jigsaw_puzzle/assets/png_pieces/CornerBottomLeft003.PNG")
const BORDER_BOTTOM_005 = preload("res://components/algorithms/jigsaw_puzzle/assets/png_pieces/BorderBottom005.PNG")
const BORDER_BOTTOM_006 = preload("res://components/algorithms/jigsaw_puzzle/assets/png_pieces/BorderBottom006.PNG")
const BORDER_LEFT_005 = preload("res://components/algorithms/jigsaw_puzzle/assets/png_pieces/BorderLeft005.PNG")
const INTERIOR_009 = preload("res://components/algorithms/jigsaw_puzzle/assets/png_pieces/Interior009.PNG")
const INTERIOR_010 = preload("res://components/algorithms/jigsaw_puzzle/assets/png_pieces/Interior010.PNG")
const BORDER_LEFT_006 = preload("res://components/algorithms/jigsaw_puzzle/assets/png_pieces/BorderLeft006.PNG")
const INTERIOR_011 = preload("res://components/algorithms/jigsaw_puzzle/assets/png_pieces/Interior011.PNG")
const INTERIOR_012 = preload("res://components/algorithms/jigsaw_puzzle/assets/png_pieces/Interior012.PNG")
const CORNER_BOTTOM_LEFT_004 = preload("res://components/algorithms/jigsaw_puzzle/assets/png_pieces/CornerBottomLeft004.PNG")
const BORDER_BOTTOM_007 = preload("res://components/algorithms/jigsaw_puzzle/assets/png_pieces/BorderBottom007.PNG")
const BORDER_BOTTOM_008 = preload("res://components/algorithms/jigsaw_puzzle/assets/png_pieces/BorderBottom008.PNG")
const BORDER_LEFT_007 = preload("res://components/algorithms/jigsaw_puzzle/assets/png_pieces/BorderLeft007.PNG")
const INTERIOR_013 = preload("res://components/algorithms/jigsaw_puzzle/assets/png_pieces/Interior013.PNG")
const INTERIOR_014 = preload("res://components/algorithms/jigsaw_puzzle/assets/png_pieces/Interior014.PNG")
const BORDER_LEFT_008 = preload("res://components/algorithms/jigsaw_puzzle/assets/png_pieces/BorderLeft008.PNG")
const INTERIOR_015 = preload("res://components/algorithms/jigsaw_puzzle/assets/png_pieces/Interior015.PNG")
const INTERIOR_016 = preload("res://components/algorithms/jigsaw_puzzle/assets/png_pieces/Interior016.PNG")

const PUZZLE_PIECE_MASK_MATERIAL = preload("res://components/algorithms/jigsaw_puzzle/puzzle_piece_mask.tres")



enum PIECE_TYPE {
	TOP_RIGHT_CORNER,
	TOP_LEFT_CORNER,
	BOTTOM_RIGHT_CORNER,
	BOTTOM_LEFT_CORNER,
	BORDER_TOP,
	BORDER_BOTTOM,
	BORDER_RIGHT,
	BORDER_LEFT,
	INTERIOR
}

var PIECES_VERTEX := {
	CORNER_TOP_RIGHT_001: {
		"top": null,
		"bottom": true,
		"right": null,
		"left": true,
	}
}

var CLASSIFIED_PIECES := {
	PIECE_TYPE.TOP_RIGHT_CORNER: [
		PuzzleMask.new(
			CORNER_TOP_RIGHT_001, 
			PieceVertex.new(PieceVertex.CONNECTOR.CORNER, PieceVertex.CONNECTOR.SLOT, PieceVertex.CONNECTOR.CORNER, PieceVertex.CONNECTOR.SLOT)
		),
		PuzzleMask.new(
			CORNER_TOP_RIGHT_002, 
			PieceVertex.new(PieceVertex.CONNECTOR.CORNER, PieceVertex.CONNECTOR.SLOT, PieceVertex.CONNECTOR.CORNER, PieceVertex.CONNECTOR.SPIKE)
		),
		PuzzleMask.new(
			CORNER_TOP_RIGHT_003, 
			PieceVertex.new(PieceVertex.CONNECTOR.CORNER, PieceVertex.CONNECTOR.SPIKE, PieceVertex.CONNECTOR.CORNER, PieceVertex.CONNECTOR.SLOT)
		),
		PuzzleMask.new(
			CORNER_TOP_RIGHT_004, 
			PieceVertex.new(PieceVertex.CONNECTOR.CORNER, PieceVertex.CONNECTOR.SPIKE, PieceVertex.CONNECTOR.CORNER, PieceVertex.CONNECTOR.SPIKE)
		)
	],
	PIECE_TYPE.TOP_LEFT_CORNER: [
		PuzzleMask.new(
			CORNER_TOP_LEFT_001, 
			PieceVertex.new(PieceVertex.CONNECTOR.CORNER, PieceVertex.CONNECTOR.SLOT, PieceVertex.CONNECTOR.SLOT, PieceVertex.CONNECTOR.CORNER)
		),
		PuzzleMask.new(
			CORNER_TOP_LEFT_002, 
			PieceVertex.new(PieceVertex.CONNECTOR.CORNER, PieceVertex.CONNECTOR.SPIKE, PieceVertex.CONNECTOR.SLOT, PieceVertex.CONNECTOR.CORNER)
		),
		PuzzleMask.new(
			CORNER_TOP_LEFT_003, 
			PieceVertex.new(PieceVertex.CONNECTOR.CORNER, PieceVertex.CONNECTOR.SLOT, PieceVertex.CONNECTOR.SPIKE, PieceVertex.CONNECTOR.CORNER)
		),
		PuzzleMask.new(
			CORNER_TOP_LEFT_004, 
			PieceVertex.new(PieceVertex.CONNECTOR.CORNER, PieceVertex.CONNECTOR.SPIKE, PieceVertex.CONNECTOR.SPIKE, PieceVertex.CONNECTOR.CORNER)
		)
	],
	
	PIECE_TYPE.BOTTOM_RIGHT_CORNER: [
		PuzzleMask.new(
			CORNER_BOTTOM_RIGHT_001, 
			PieceVertex.new(PieceVertex.CONNECTOR.SLOT, PieceVertex.CONNECTOR.CORNER, PieceVertex.CONNECTOR.CORNER, PieceVertex.CONNECTOR.SLOT)
		),
		PuzzleMask.new(
			CORNER_BOTTOM_RIGHT_002, 
			PieceVertex.new(PieceVertex.CONNECTOR.SLOT, PieceVertex.CONNECTOR.CORNER, PieceVertex.CONNECTOR.CORNER, PieceVertex.CONNECTOR.SPIKE)
		),
		PuzzleMask.new(
			CORNER_BOTTOM_RIGHT_003, 
			PieceVertex.new(PieceVertex.CONNECTOR.SPIKE, PieceVertex.CONNECTOR.CORNER, PieceVertex.CONNECTOR.CORNER, PieceVertex.CONNECTOR.SLOT)
		),
		PuzzleMask.new(
			CORNER_BOTTOM_RIGHT_004, 
			PieceVertex.new(PieceVertex.CONNECTOR.SPIKE, PieceVertex.CONNECTOR.CORNER, PieceVertex.CONNECTOR.CORNER, PieceVertex.CONNECTOR.SPIKE)
		)
	],
	PIECE_TYPE.BOTTOM_LEFT_CORNER: [
		PuzzleMask.new(
			CORNER_BOTTOM_LEFT_001, 
			PieceVertex.new(PieceVertex.CONNECTOR.SLOT, PieceVertex.CONNECTOR.CORNER, PieceVertex.CONNECTOR.SLOT, PieceVertex.CONNECTOR.CORNER)
		),
		PuzzleMask.new(
			CORNER_BOTTOM_LEFT_002, 
			PieceVertex.new(PieceVertex.CONNECTOR.SLOT, PieceVertex.CONNECTOR.CORNER, PieceVertex.CONNECTOR.SPIKE, PieceVertex.CONNECTOR.CORNER)
		),
		PuzzleMask.new(
			CORNER_BOTTOM_LEFT_003, 
			PieceVertex.new(PieceVertex.CONNECTOR.SPIKE, PieceVertex.CONNECTOR.CORNER, PieceVertex.CONNECTOR.SLOT, PieceVertex.CONNECTOR.CORNER)
		),
		PuzzleMask.new(
			CORNER_BOTTOM_LEFT_004, 
			PieceVertex.new(PieceVertex.CONNECTOR.SPIKE, PieceVertex.CONNECTOR.CORNER, PieceVertex.CONNECTOR.SPIKE, PieceVertex.CONNECTOR.CORNER)
		)
	],
	PIECE_TYPE.BORDER_TOP: [
		PuzzleMask.new(
			BORDER_TOP_001, 
			PieceVertex.new(PieceVertex.CONNECTOR.CORNER, PieceVertex.CONNECTOR.SLOT, PieceVertex.CONNECTOR.SLOT, PieceVertex.CONNECTOR.SLOT)
		),
		PuzzleMask.new(
			BORDER_TOP_002, 
			PieceVertex.new(PieceVertex.CONNECTOR.CORNER, PieceVertex.CONNECTOR.SLOT, PieceVertex.CONNECTOR.SLOT, PieceVertex.CONNECTOR.SPIKE)
		),
		PuzzleMask.new(
			BORDER_TOP_003, 
			PieceVertex.new(PieceVertex.CONNECTOR.SPIKE, PieceVertex.CONNECTOR.SLOT, PieceVertex.CONNECTOR.SLOT, PieceVertex.CONNECTOR.SLOT)
		),
		PuzzleMask.new(
			BORDER_TOP_004, 
			PieceVertex.new(PieceVertex.CONNECTOR.CORNER, PieceVertex.CONNECTOR.SPIKE, PieceVertex.CONNECTOR.SLOT, PieceVertex.CONNECTOR.SPIKE)
		),
		PuzzleMask.new(
			BORDER_TOP_005, 
			PieceVertex.new(PieceVertex.CONNECTOR.CORNER, PieceVertex.CONNECTOR.SLOT, PieceVertex.CONNECTOR.SPIKE, PieceVertex.CONNECTOR.SLOT)
		),
		PuzzleMask.new(
			BORDER_TOP_006, 
			PieceVertex.new(PieceVertex.CONNECTOR.CORNER, PieceVertex.CONNECTOR.SLOT, PieceVertex.CONNECTOR.SPIKE, PieceVertex.CONNECTOR.SPIKE)
		),
		PuzzleMask.new(
			BORDER_TOP_007, 
			PieceVertex.new(PieceVertex.CONNECTOR.CORNER, PieceVertex.CONNECTOR.SPIKE, PieceVertex.CONNECTOR.SPIKE, PieceVertex.CONNECTOR.SLOT)
		),
		PuzzleMask.new(
			BORDER_TOP_008, 
			PieceVertex.new(PieceVertex.CONNECTOR.CORNER, PieceVertex.CONNECTOR.SPIKE, PieceVertex.CONNECTOR.SPIKE, PieceVertex.CONNECTOR.SPIKE)
		)
	],
	PIECE_TYPE.BORDER_BOTTOM: [
		PuzzleMask.new(
			BORDER_BOTTOM_001, 
			PieceVertex.new(PieceVertex.CONNECTOR.SLOT, PieceVertex.CONNECTOR.CORNER, PieceVertex.CONNECTOR.SLOT, PieceVertex.CONNECTOR.SLOT)
		),
		PuzzleMask.new(
			BORDER_BOTTOM_002, 
			PieceVertex.new(PieceVertex.CONNECTOR.SLOT, PieceVertex.CONNECTOR.CORNER, PieceVertex.CONNECTOR.SLOT, PieceVertex.CONNECTOR.SPIKE)
		),
		PuzzleMask.new(
			BORDER_BOTTOM_003, 
			PieceVertex.new(PieceVertex.CONNECTOR.SLOT, PieceVertex.CONNECTOR.CORNER, PieceVertex.CONNECTOR.SPIKE, PieceVertex.CONNECTOR.SLOT)
		),
		PuzzleMask.new(
			BORDER_BOTTOM_004, 
			PieceVertex.new(PieceVertex.CONNECTOR.SLOT, PieceVertex.CONNECTOR.CORNER, PieceVertex.CONNECTOR.SPIKE, PieceVertex.CONNECTOR.SPIKE)
		),
		PuzzleMask.new(
			BORDER_BOTTOM_005, 
			PieceVertex.new(PieceVertex.CONNECTOR.SPIKE, PieceVertex.CONNECTOR.CORNER, PieceVertex.CONNECTOR.SLOT, PieceVertex.CONNECTOR.SLOT)
		),
		PuzzleMask.new(
			BORDER_BOTTOM_006, 
			PieceVertex.new(PieceVertex.CONNECTOR.SPIKE, PieceVertex.CONNECTOR.CORNER, PieceVertex.CONNECTOR.SLOT, PieceVertex.CONNECTOR.SPIKE)
		),
		PuzzleMask.new(
			BORDER_BOTTOM_007, 
			PieceVertex.new(PieceVertex.CONNECTOR.SPIKE, PieceVertex.CONNECTOR.CORNER, PieceVertex.CONNECTOR.SPIKE, PieceVertex.CONNECTOR.SLOT)
		), 
		PuzzleMask.new(
			BORDER_BOTTOM_008, 
			PieceVertex.new(PieceVertex.CONNECTOR.SPIKE, PieceVertex.CONNECTOR.CORNER, PieceVertex.CONNECTOR.SPIKE, PieceVertex.CONNECTOR.SPIKE)
		)
	],
	PIECE_TYPE.BORDER_LEFT: [
		PuzzleMask.new(
			BORDER_LEFT_001, 
			PieceVertex.new(PieceVertex.CONNECTOR.SLOT, PieceVertex.CONNECTOR.SLOT, PieceVertex.CONNECTOR.SLOT, PieceVertex.CONNECTOR.CORNER)
		),
		PuzzleMask.new(
			BORDER_LEFT_002, 
			PieceVertex.new(PieceVertex.CONNECTOR.SLOT, PieceVertex.CONNECTOR.SPIKE, PieceVertex.CONNECTOR.SLOT, PieceVertex.CONNECTOR.CORNER)
		),
		PuzzleMask.new(
			BORDER_LEFT_003, 
			PieceVertex.new(PieceVertex.CONNECTOR.SLOT, PieceVertex.CONNECTOR.SLOT, PieceVertex.CONNECTOR.SPIKE, PieceVertex.CONNECTOR.CORNER)
		),
		PuzzleMask.new(
			BORDER_LEFT_004, 
			PieceVertex.new(PieceVertex.CONNECTOR.SLOT, PieceVertex.CONNECTOR.SPIKE, PieceVertex.CONNECTOR.SPIKE, PieceVertex.CONNECTOR.CORNER)
		),
		PuzzleMask.new(
			BORDER_LEFT_005, 
			PieceVertex.new(PieceVertex.CONNECTOR.SPIKE, PieceVertex.CONNECTOR.SLOT, PieceVertex.CONNECTOR.SLOT, PieceVertex.CONNECTOR.CORNER)
		),
		PuzzleMask.new(
			BORDER_LEFT_006, 
			PieceVertex.new(PieceVertex.CONNECTOR.SPIKE, PieceVertex.CONNECTOR.SPIKE, PieceVertex.CONNECTOR.SLOT, PieceVertex.CONNECTOR.CORNER)
		),
		PuzzleMask.new(
			BORDER_LEFT_007, 
			PieceVertex.new(PieceVertex.CONNECTOR.SPIKE, PieceVertex.CONNECTOR.SLOT, PieceVertex.CONNECTOR.SPIKE, PieceVertex.CONNECTOR.CORNER)
		),
		PuzzleMask.new(
			BORDER_LEFT_008, 
			PieceVertex.new(PieceVertex.CONNECTOR.SPIKE, PieceVertex.CONNECTOR.SPIKE, PieceVertex.CONNECTOR.SPIKE, PieceVertex.CONNECTOR.CORNER)
		)
],
	PIECE_TYPE.BORDER_RIGHT: [
		PuzzleMask.new(
			BORDER_RIGHT_001, 
			PieceVertex.new(PieceVertex.CONNECTOR.SLOT, PieceVertex.CONNECTOR.SLOT, PieceVertex.CONNECTOR.CORNER, PieceVertex.CONNECTOR.SLOT)
		),
		PuzzleMask.new(
			BORDER_RIGHT_002, 
			PieceVertex.new(PieceVertex.CONNECTOR.SLOT, PieceVertex.CONNECTOR.SLOT, PieceVertex.CONNECTOR.CORNER, PieceVertex.CONNECTOR.SPIKE)
		),
		PuzzleMask.new(
			BORDER_RIGHT_003, 
			PieceVertex.new(PieceVertex.CONNECTOR.SLOT, PieceVertex.CONNECTOR.SPIKE, PieceVertex.CONNECTOR.CORNER, PieceVertex.CONNECTOR.SLOT)
		),
		PuzzleMask.new(
			BORDER_RIGHT_004, 
			PieceVertex.new(PieceVertex.CONNECTOR.SLOT, PieceVertex.CONNECTOR.SPIKE, PieceVertex.CONNECTOR.CORNER, PieceVertex.CONNECTOR.SPIKE)
		),
		PuzzleMask.new(
			BORDER_RIGHT_005, 
			PieceVertex.new(PieceVertex.CONNECTOR.SPIKE, PieceVertex.CONNECTOR.SLOT, PieceVertex.CONNECTOR.CORNER, PieceVertex.CONNECTOR.SLOT)
		),
		PuzzleMask.new(
			BORDER_RIGHT_006, 
			PieceVertex.new(PieceVertex.CONNECTOR.SPIKE, PieceVertex.CONNECTOR.SLOT, PieceVertex.CONNECTOR.CORNER, PieceVertex.CONNECTOR.SPIKE)
		),
		PuzzleMask.new(
			BORDER_RIGHT_007, 
			PieceVertex.new(PieceVertex.CONNECTOR.SPIKE, PieceVertex.CONNECTOR.SPIKE, PieceVertex.CONNECTOR.CORNER, PieceVertex.CONNECTOR.SLOT)
		),
		PuzzleMask.new(
			BORDER_RIGHT_008, 
			PieceVertex.new(PieceVertex.CONNECTOR.SPIKE, PieceVertex.CONNECTOR.SPIKE, PieceVertex.CONNECTOR.CORNER, PieceVertex.CONNECTOR.SPIKE)
		)
	],
	PIECE_TYPE.INTERIOR: [
		PuzzleMask.new(
			INTERIOR_001, 
			PieceVertex.new(PieceVertex.CONNECTOR.SLOT, PieceVertex.CONNECTOR.SLOT, PieceVertex.CONNECTOR.SLOT, PieceVertex.CONNECTOR.SLOT)
		),
		PuzzleMask.new(
			INTERIOR_002, 
			PieceVertex.new(PieceVertex.CONNECTOR.SLOT, PieceVertex.CONNECTOR.SLOT, PieceVertex.CONNECTOR.SLOT, PieceVertex.CONNECTOR.SPIKE)
		),
		PuzzleMask.new(
			INTERIOR_003, 
			PieceVertex.new(PieceVertex.CONNECTOR.SLOT, PieceVertex.CONNECTOR.SPIKE, PieceVertex.CONNECTOR.SLOT, PieceVertex.CONNECTOR.SLOT)
		),
		PuzzleMask.new(
			INTERIOR_004, 
			PieceVertex.new(PieceVertex.CONNECTOR.SLOT, PieceVertex.CONNECTOR.SPIKE, PieceVertex.CONNECTOR.SLOT, PieceVertex.CONNECTOR.SPIKE)
		),
		PuzzleMask.new(
			INTERIOR_005, 
			PieceVertex.new(PieceVertex.CONNECTOR.SLOT, PieceVertex.CONNECTOR.SLOT, PieceVertex.CONNECTOR.SPIKE, PieceVertex.CONNECTOR.SLOT)
		),
		PuzzleMask.new(
			INTERIOR_006, 
			PieceVertex.new(PieceVertex.CONNECTOR.SLOT, PieceVertex.CONNECTOR.SLOT, PieceVertex.CONNECTOR.SPIKE, PieceVertex.CONNECTOR.SPIKE)
		),
		PuzzleMask.new(
			INTERIOR_007, 
			PieceVertex.new(PieceVertex.CONNECTOR.SLOT, PieceVertex.CONNECTOR.SPIKE, PieceVertex.CONNECTOR.SPIKE, PieceVertex.CONNECTOR.SLOT)
		),
		PuzzleMask.new(
			INTERIOR_008, 
			PieceVertex.new(PieceVertex.CONNECTOR.SLOT, PieceVertex.CONNECTOR.SPIKE, PieceVertex.CONNECTOR.SPIKE, PieceVertex.CONNECTOR.SPIKE)
		),
		PuzzleMask.new(
			INTERIOR_009, 
			PieceVertex.new(PieceVertex.CONNECTOR.SPIKE, PieceVertex.CONNECTOR.SLOT, PieceVertex.CONNECTOR.SLOT, PieceVertex.CONNECTOR.SLOT)
		),
		PuzzleMask.new(
			INTERIOR_010, 
			PieceVertex.new(PieceVertex.CONNECTOR.SPIKE, PieceVertex.CONNECTOR.SLOT, PieceVertex.CONNECTOR.SLOT, PieceVertex.CONNECTOR.SPIKE)
		),
		PuzzleMask.new(
			INTERIOR_011, 
			PieceVertex.new(PieceVertex.CONNECTOR.SPIKE, PieceVertex.CONNECTOR.SPIKE, PieceVertex.CONNECTOR.SLOT, PieceVertex.CONNECTOR.SLOT)
		),
		PuzzleMask.new(
			INTERIOR_012, 
			PieceVertex.new(PieceVertex.CONNECTOR.SPIKE, PieceVertex.CONNECTOR.SPIKE, PieceVertex.CONNECTOR.SLOT, PieceVertex.CONNECTOR.SPIKE)
		),
		PuzzleMask.new(
			INTERIOR_013, 
			PieceVertex.new(PieceVertex.CONNECTOR.SPIKE, PieceVertex.CONNECTOR.SLOT, PieceVertex.CONNECTOR.SPIKE, PieceVertex.CONNECTOR.SLOT)
		),
		PuzzleMask.new(
			INTERIOR_014, 
			PieceVertex.new(PieceVertex.CONNECTOR.SPIKE, PieceVertex.CONNECTOR.SLOT, PieceVertex.CONNECTOR.SPIKE, PieceVertex.CONNECTOR.SPIKE)
		),
		PuzzleMask.new(
			INTERIOR_015, 
			PieceVertex.new(PieceVertex.CONNECTOR.SPIKE, PieceVertex.CONNECTOR.SPIKE, PieceVertex.CONNECTOR.SPIKE, PieceVertex.CONNECTOR.SLOT)
		),
		PuzzleMask.new(
			INTERIOR_016, 
			PieceVertex.new(PieceVertex.CONNECTOR.SPIKE, PieceVertex.CONNECTOR.SPIKE, PieceVertex.CONNECTOR.SPIKE, PieceVertex.CONNECTOR.SPIKE)
		)
	]
}

func interior_pieces() -> Array:
	return CLASSIFIED_PIECES[PIECE_TYPE.INTERIOR]


func top_left_corner_pieces() -> Array:
	return CLASSIFIED_PIECES[PIECE_TYPE.TOP_LEFT_CORNER]


func top_right_corner_pieces() -> Array:
	return CLASSIFIED_PIECES[PIECE_TYPE.TOP_RIGHT_CORNER]


func bottom_left_corner_pieces() -> Array:
	return CLASSIFIED_PIECES[PIECE_TYPE.BOTTOM_LEFT_CORNER]


func bottom_right_corner_pieces() -> Array:
	return CLASSIFIED_PIECES[PIECE_TYPE.BOTTOM_RIGHT_CORNER]
	
	
func border_bottom_pieces() -> Array:
	return CLASSIFIED_PIECES[PIECE_TYPE.BORDER_BOTTOM]
	

func border_top_pieces() -> Array:
	return CLASSIFIED_PIECES[PIECE_TYPE.BORDER_TOP]
	
	
func border_left_pieces() -> Array:
	return CLASSIFIED_PIECES[PIECE_TYPE.BORDER_LEFT]
	
	
func border_right_pieces() -> Array:
	return CLASSIFIED_PIECES[PIECE_TYPE.BORDER_RIGHT]
	

class PuzzleMask:
	var texture: CompressedTexture2D
	var vertex: PieceVertex
	
	func _init(_texture: CompressedTexture2D, _vertex: PieceVertex):
		texture = _texture
		vertex = _vertex
	

class PieceVertex:
	enum CONNECTOR {
		SLOT,
		SPIKE,
		CORNER
	}
	
	var top: CONNECTOR
	var bottom: CONNECTOR
	var right: CONNECTOR
	var left: CONNECTOR
	
	
	func _init(_top: CONNECTOR, _bottom: CONNECTOR, _right: CONNECTOR, _left: CONNECTOR):
		top = _top
		bottom = _bottom
		right = _right
		left = _left
	
	
	func can_connect_with(vertex: PieceVertex) -> bool:
		return can_connect_on_top(vertex) \
			or can_connect_on_bottom(vertex) \
			or can_connect_on_right(vertex) \
			or can_connect_on_left(vertex)
			
			
	func can_connect_on_top(vertex: PieceVertex) -> bool:
		return top != CONNECTOR.CORNER and (top == CONNECTOR.SLOT and vertex.bottom == CONNECTOR.SPIKE) \
			or (top == CONNECTOR.SPIKE and vertex.bottom == CONNECTOR.SLOT)
			
			
	func can_connect_on_bottom(vertex: PieceVertex) -> bool:
		return bottom != CONNECTOR.CORNER and (bottom == CONNECTOR.SLOT and vertex.top == CONNECTOR.SPIKE) \
			or (bottom == CONNECTOR.SPIKE and vertex.top == CONNECTOR.SLOT)


	func can_connect_on_right(vertex: PieceVertex) -> bool:
		return right != CONNECTOR.CORNER and (right == CONNECTOR.SLOT and vertex.left == CONNECTOR.SPIKE) \
			or (right == CONNECTOR.SPIKE and vertex.left == CONNECTOR.SLOT)
			
			
	func can_connect_on_left(vertex: PieceVertex) -> bool:
		return left != CONNECTOR.CORNER and (left == CONNECTOR.SLOT and vertex.right == CONNECTOR.SPIKE) \
			or (left == CONNECTOR.SPIKE and vertex.right == CONNECTOR.SLOT)
			
	
	func is_top_left_corner() -> bool:
		return top == CONNECTOR.CORNER and left == CONNECTOR.CORNER
	
	
	func is_top_right_corner() -> bool:
		return top == CONNECTOR.CORNER and right == CONNECTOR.CORNER
	
	
	func is_bottom_right_corner() -> bool:
		return bottom == CONNECTOR.CORNER and right == CONNECTOR.CORNER
	
	
	func is_bottom_left_corner() -> bool:
		return bottom == CONNECTOR.CORNER and left == CONNECTOR.CORNER
		
		
	func is_top_border() -> bool:
		return top == CONNECTOR.CORNER \
			and bottom != CONNECTOR.CORNER \
			and right != CONNECTOR.CORNER \
			and left != CONNECTOR.CORNER
	
	
	func is_bottom_border() -> bool:
		return bottom == CONNECTOR.CORNER \
			and top != CONNECTOR.CORNER \
			and right != CONNECTOR.CORNER \
			and left != CONNECTOR.CORNER
	
	
	func is_right_border() -> bool:
		return right == CONNECTOR.CORNER \
			and top != CONNECTOR.CORNER \
			and bottom != CONNECTOR.CORNER \
			and left != CONNECTOR.CORNER
	
	
	func is_left_border() -> bool:
		return left == CONNECTOR.CORNER \
			and top != CONNECTOR.CORNER \
			and bottom != CONNECTOR.CORNER \
			and right != CONNECTOR.CORNER
	
	
	func is_interior() -> bool:
		return left != CONNECTOR.CORNER \
			and top != CONNECTOR.CORNER \
			and bottom != CONNECTOR.CORNER \
			and right != CONNECTOR.CORNER
