extends Panel


func _can_drop_data(at_position: Vector2, data):
	return data is PuzzlePiece


func _drop_data(at_position: Vector2, data):
	if data is PuzzlePiece:
		data.reparent(self)
		var size = data.texture_size()
		data.position = at_position - Vector2(size.x / 2, size.y / 2) * data.scale
