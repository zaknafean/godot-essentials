class_name PuzzlePiece extends TextureRect

enum NEIGHBOUR {
	RIGHT,
	LEFT,
	BOTTOM,
	TOP
}

const GROUP_NAME = "puzzle-piece"

## Dictionary <enum, PuzzlePiece>
var neighbours := {
	NEIGHBOUR.TOP: null,
	NEIGHBOUR.BOTTOM: null,
	NEIGHBOUR.RIGHT: null,
	NEIGHBOUR.LEFT: null
}

var puzzle_mask: JigsawPiecesLoader.PuzzleMask
var column: int
var row: int
var puzzle_size: Vector2

var puzzle_grid := []
var drag_active := false
var is_combined := false


func _init(_column: int, _row: int, _size: Vector2, image: Texture2D, _puzzle_mask: JigsawPiecesLoader.PuzzleMask):
	column = _column
	row = _row
	puzzle_size = _size
	texture = image
	puzzle_mask = _puzzle_mask
	

func _enter_tree():
	drag_active = true
	name = "PuzzlePiece_Column%d_Row%d" % [column, row]
	add_to_group(GROUP_NAME)


func _ready():
	if not puzzle_grid.is_empty():
		prepare_neighbours()
	

func _notification(notification_type):
	match notification_type:
		NOTIFICATION_DRAG_END:
			if drag_active:
				show()


func set_grid(grid: Array) -> void:
	puzzle_grid = grid


func texture_size() -> Vector2:
	return texture.get_size()


func _get_drag_data(_at_position: Vector2) -> PuzzlePiece:
	if drag_active:
		var parent_node_preview := Control.new()
		var piece_preview := TextureRect.new()
		
		parent_node_preview.scale = scale
		piece_preview.texture = texture
		piece_preview.expand_mode = expand_mode
		piece_preview.material = material
		
		parent_node_preview.add_child(piece_preview)
		piece_preview.position = -texture_size() / 2
		set_drag_preview(parent_node_preview)
		
		hide()
		
		return self
		
	return null


func prepare_neighbours() -> void:
	if is_top_left_corner():
		neighbours[NEIGHBOUR.RIGHT] = puzzle_grid[1][0]
		neighbours[NEIGHBOUR.BOTTOM] = puzzle_grid[0][1]
		
	if is_top_right_corner():
		neighbours[NEIGHBOUR.LEFT] = puzzle_grid[column - 1][0]
		neighbours[NEIGHBOUR.BOTTOM] = puzzle_grid[column][row + 1]

	if is_bottom_left_corner():
		neighbours[NEIGHBOUR.RIGHT] = puzzle_grid[1][row]
		neighbours[NEIGHBOUR.TOP] = puzzle_grid[0][row - 1]

	if is_bottom_right_corner():
		neighbours[NEIGHBOUR.LEFT] = puzzle_grid[column - 1][row]
		neighbours[NEIGHBOUR.TOP] = puzzle_grid[column][row - 1]
		
	if is_border_top():
		neighbours[NEIGHBOUR.LEFT] = puzzle_grid[column - 1][row]
		neighbours[NEIGHBOUR.RIGHT] = puzzle_grid[column + 1][row]
		neighbours[NEIGHBOUR.BOTTOM] = puzzle_grid[column][row + 1]

	if is_border_bottom():
		neighbours[NEIGHBOUR.LEFT] = puzzle_grid[column - 1][row]
		neighbours[NEIGHBOUR.RIGHT] = puzzle_grid[column + 1][row]
		neighbours[NEIGHBOUR.TOP] = puzzle_grid[column][row - 1]

	if is_border_left():
		neighbours[NEIGHBOUR.RIGHT] = puzzle_grid[column + 1][row]
		neighbours[NEIGHBOUR.TOP] = puzzle_grid[column][row - 1]
		neighbours[NEIGHBOUR.BOTTOM] = puzzle_grid[column][row + 1]

	if is_border_right():
		neighbours[NEIGHBOUR.LEFT] = puzzle_grid[column - 1][row]
		neighbours[NEIGHBOUR.TOP] = puzzle_grid[column][row - 1]
		neighbours[NEIGHBOUR.BOTTOM] = puzzle_grid[column][row + 1]

	if is_interior():
		neighbours[NEIGHBOUR.LEFT] = puzzle_grid[column - 1][row]
		neighbours[NEIGHBOUR.RIGHT] = puzzle_grid[column + 1][row]
		neighbours[NEIGHBOUR.TOP] = puzzle_grid[column][row - 1]
		neighbours[NEIGHBOUR.BOTTOM] = puzzle_grid[column][row + 1]


func has_neighbour_top() -> bool:
	return neighbours[NEIGHBOUR.TOP] is PuzzlePiece


func has_neighbour_bottom() -> bool:
	return neighbours[NEIGHBOUR.BOTTOM] is PuzzlePiece


func has_neighbour_right() -> bool:
	return neighbours[NEIGHBOUR.RIGHT] is PuzzlePiece


func has_neighbour_left() -> bool:
	return neighbours[NEIGHBOUR.LEFT] is PuzzlePiece


func is_interior() -> bool:
	return not is_corner() and not is_border()


func is_corner() -> bool:
	return is_top_left_corner() \
		or is_bottom_left_corner() \
		or is_top_right_corner() \
		or is_bottom_right_corner()


func is_border() -> bool:
	return is_border_top() \
		or is_border_bottom() \
		or is_border_left() \
		or is_border_right()


func is_border_top() -> bool:
	return column > 0 and column < puzzle_size.x - 1 and row == 0


func is_border_bottom() -> bool:
	return column > 0 and column < puzzle_size.x - 1 and row == puzzle_size.y - 1


func is_border_left() -> bool:
	return row > 0 and row < puzzle_size.y - 1 and column == 0


func is_border_right() -> bool:
	return row > 0 and row < puzzle_size.y - 1 and column == puzzle_size.x - 1


func is_top_left_corner() -> bool:
	return column == 0 and row == 0
	

func is_bottom_left_corner() -> bool:
	return column == 0 and row == puzzle_size.y - 1
	

func is_top_right_corner() -> bool:
	return column == puzzle_size.x - 1 and row == 0
	

func is_bottom_right_corner() -> bool:
	return column == puzzle_size.x - 1 and row == puzzle_size.y - 1
