extends Node

signal frame_freezed_started
signal frame_freezed_finished


func mouse_screen_position() -> Vector2:
	return get_viewport().get_mouse_position()


func start_frame_freeze(time_scale: float, duration: float) -> void:
	frame_freezed_started.emit()
	
	var original_time_scale_value := Engine.time_scale
	Engine.time_scale = time_scale
	
	await get_tree().create_timer(duration * time_scale).timeout
	Engine.time_scale = original_time_scale_value
	
	frame_freezed_finished.emit()

	
func open_external_link(url: String) -> void:
	if StringWizard.is_valid_url(url) and OS.has_method("shell_open"):
		if OS.get_name() == "Web":
			url = url.uri_encode()
			
		OS.shell_open(url)


func random_enum(_enum):
	return _enum.keys()[randi() % _enum.size()]
	
	
func get_texture_dimensions(texture: Texture2D) -> Rect2i:
	var image: Image = texture.get_image()
	return image.get_used_rect()
	
	
func get_texture_rect_dimensions(texture_rect: TextureRect) -> Vector2:
	var texture: Texture2D = texture_rect.texture
	var used_rect := get_texture_dimensions(texture)
	var texture_dimensions := Vector2(used_rect.size) * texture_rect.scale

	return texture_dimensions


func get_sprite_dimensions(sprite: Sprite2D) -> Vector2:
	var texture: Texture2D = sprite.texture
	var used_rect := get_texture_dimensions(texture)
	var sprite_dimensions := Vector2(used_rect.size) * sprite.scale

	return sprite_dimensions


func screenshot_to_texture_rect(texture_rect: TextureRect) -> TextureRect:
	var img = get_viewport().get_texture().get_image()
	var tex = ImageTexture.create_from_image(img)
	texture_rect.set_texture(tex)
	
	return texture_rect
