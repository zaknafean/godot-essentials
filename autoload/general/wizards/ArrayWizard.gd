class_name ArrayWizard

	
## Flatten any array with n dimensions recursively
static func flatten(array: Array, result = []):
	for i in array.size():
		if typeof(array[i]) >= TYPE_ARRAY:
			flatten(array[i], result)
		else:
			result.append(array[i])

	return result


static func pick_random_values(array: Array, items_to_pick: int = 1, duplicates: bool = true) -> Array:
	var result := []
	var target = flatten(array.duplicate())
	target.shuffle()
	
	items_to_pick = min(target.size(), items_to_pick)
	
	for i in range(items_to_pick):
		var item = target.pick_random()
		result.append(item)

		if not duplicates:
			target.erase(item)
		
	return result
	
